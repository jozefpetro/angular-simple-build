import gulp from 'gulp';
import path from 'path';
import webpack from 'webpack-stream';
const browserSync = require('browser-sync');

const reload = () => browserSync.reload();
const root = 'src';
const dist = 'dist';

// helper method for resolving paths
const resolveToApp = (glob) => {
    glob = glob || '';
    return path.join(root, 'app', glob); // app/{glob}
};

const resolveToRoot = (glob) => {
    glob = glob || '';
    return path.join(root, glob); // src/{glob}
}

// map of all paths
const paths = {
    js: resolveToApp('**/*!(.spec.js).js'), // exclude spec files
    styl: resolveToRoot('**/*.scss'), // stylesheets
    html: [
        resolveToRoot('**/**.html'),
        resolveToRoot('index.html')
    ],
    entry: path.join(root, 'app/app.module.js'),
    output: dist
};

gulp.task('webpack', () => {
    return gulp.src(paths.entry)
        .pipe(webpack(require('./webpack.config')))
        .on('error', function handleError() {
            this.emit('end'); // Recover from errors
        })
        .pipe(gulp.dest(paths.output));
});

gulp.task('reload', ['webpack'], (done) => {
    reload();
done();
});

gulp.task('serve', ['webpack'], () => {
    browserSync({
        port: process.env.PORT || 3000,
    open: false,
    server: { baseDir: dist }
});
});

gulp.task('watch', ['serve'], () => {
    const allPaths = [].concat([paths.js], paths.html, [paths.styl]);
gulp.watch(allPaths, ['reload']);
});

gulp.task('default', ['watch']);
