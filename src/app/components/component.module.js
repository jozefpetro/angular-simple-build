import angular from 'angular';

import test from './test/test'

const ComponentsModule = angular.module('Components', [])
    .component('test', test);

export default ComponentsModule;