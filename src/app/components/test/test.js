import './test.scss';
import template from './test.html';
import controller from './test.ctrl';

const testComponent = {
    template: template,
    controller: controller,
    controllerAs: 'test'
};

export default testComponent;