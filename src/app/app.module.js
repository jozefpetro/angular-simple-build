import angular from 'angular';

//local modules
import Components from './components/component.module';

//root level component
import appComponent from './app.component';


const AppModule = angular.module('App',[
    Components.name
]);

AppModule.component('app', appComponent);

