import './app.scss';
import template from './app.html';
import controller from './app.ctrl';

const appComponent = {
    template: template,
    controller: controller,
    controllerAs: 'app'
};

export default appComponent;