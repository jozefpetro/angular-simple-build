'use strict';

//load modules


import HtmlWebpackPlugin from 'html-webpack-plugin';
/*

import ExtractTextPlugin from 'extract-text-webpack-plugin'
import CopyWebpackPlugin from 'copy-webpack-plugin'
import webpack from 'webpack';
*/

module.exports = {
    devtool: 'sourcemap',
    output: {
        filename: 'app.js',
    },
    module: {
        loaders: [
            { test: /\.js$/, exclude: [/node_modules/], loader: 'ng-annotate!babel' },
            { test: /\.(scss|css)$/, loader: 'style!css!sass' },
            { test: /\.html$/, loader: 'html-loader' },
            //{ test: /\.styl$/, loader: 'style!css!stylus' },
            //{ test: /\.scss$/, loader: ExtractTextPlugin.extract('style', 'css!sass?sourceMap') },
            { test: /\.woff(2)?(\?v=[0-9]+\.[0-9]+\.[0-9]+)?$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },
            { test: /\.(ttf|otf|png|jpe?g|eot|svg)(\?v=[0-9]+\.[0-9]+\.[0-9]+)?$/, loader: 'file-loader' },
            //{include: /\.json$/, loaders: ['json-loader']},
        ]
    },
    plugins: [


        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: 'body'
        })

        /*

         new ExtractTextPlugin("styles.css"),

        new CopyWebpackPlugin([{
            from: __dirname + '/src/images',
            to: 'images'
        }]),

        new CopyWebpackPlugin([{
            from: __dirname + '/src/fonts',
            to: 'fonts'
        }]),

        new CopyWebpackPlugin([{
            from: __dirname + '/src/views',
            to: 'views'
        }]),

        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        })
*/
    ]
};
